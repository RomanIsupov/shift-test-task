package ru.romanisupov.files;

import ru.romanisupov.exception.CreateFileException;
import ru.romanisupov.exception.WriteToFileException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

final class FilesWriter {

    void write(Path path, List<String> values) {
        var newFile = createFile(path).toFile();
        try (FileOutputStream fileOutputStream = new FileOutputStream(newFile)) {
            for (var value : values) {
                var valueInBytes = String.format("%s%n", value).getBytes();
                fileOutputStream.write(valueInBytes);
            }
        } catch (IOException e) {
            throw new WriteToFileException(newFile.toPath());
        }
    }

    private Path createFile(Path path) {
        try {
            return Files.createFile(path);
        } catch (IOException e) {
            throw new CreateFileException(path);
        }
    }
}

package ru.romanisupov.files;

import ru.romanisupov.configuration.DataType;

import java.nio.file.Path;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FilesResolver {

    private final Map<DataType, FilesReader> filesReaders;

    private final FilesWriter filesWriter;

    public FilesResolver() {
        filesReaders = new EnumMap<>(DataType.class);
        for (var value : DataType.values()) {
            filesReaders.put(value, new FilesReader(value));
        }
        filesWriter = new FilesWriter();
    }

    public List<Object[]> resolveReading(List<Path> paths, DataType dataType) {
        var filesReader = filesReaders.get(dataType);
        return paths.stream()
                .map(filesReader::read)
                .collect(Collectors.toList());
    }

    public void resolveWriting(Path path, List<String> values) {
        filesWriter.write(path, values);
    }
}

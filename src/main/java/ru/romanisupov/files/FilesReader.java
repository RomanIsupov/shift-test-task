package ru.romanisupov.files;

import ru.romanisupov.configuration.DataType;
import ru.romanisupov.exception.IncorrectValueException;
import ru.romanisupov.exception.ReadFileException;
import ru.romanisupov.exception.TypeNotSpecifiedException;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

final class FilesReader {

    private final DataType dataType;

    FilesReader(DataType dataType) {
        if (dataType == null) {
            throw new TypeNotSpecifiedException();
        }
        this.dataType = dataType;
    }

    Object[] read(Path path) {
        try (BufferedReader bufferedReader = Files.newBufferedReader(path)) {
            return readFileData(bufferedReader).toArray();
        } catch (IOException e) {
            throw new ReadFileException(path);
        }
    }

    private List<Object> readFileData(BufferedReader bufferedReader) throws IOException {
        List<Object> fileData = new ArrayList<>();
        String currentValue = bufferedReader.readLine();
        while (currentValue != null) {
            addValue(fileData, currentValue);
            currentValue = bufferedReader.readLine();
        }
        return fileData;
    }

    private void addValue(List<Object> values, String value) {
        try {
            values.add(validate(value));
        } catch (IncorrectValueException e) {
            System.out.println(e.getMessage());
        }
    }

    private Object validate(String value) {
        if (dataType == DataType.STRING) {
            return validateString(value);
        } else {
            return validateInteger(value);
        }
    }

    private String validateString(String value) {
        if (value.isEmpty() || value.contains(" ")) {
            throw new IncorrectValueException(value);
        }
        return value;
    }

    private Integer validateInteger(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new IncorrectValueException(value);
        }
    }
}

package ru.romanisupov;

public class Application {

    public static void main(String[] args) {
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.dispatch(args);
    }
}
package ru.romanisupov.sorting;

import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.function.BiPredicate;

@AllArgsConstructor
class Sorter {

    private final BiPredicate<Object, Object> biPredicate;

    void sort(Object[] array) {
        sort(array, 0, array.length - 1);
    }

    Object[] merge(Object[] array1, Object[] array2) {
        Object[] resultArray = new Object[array1.length + array2.length];
        merge(resultArray, 0, array1, array2);
        return resultArray;
    }

    private void sort(Object[] array, int leftIndex, int rightIndex) {
        if (leftIndex < rightIndex) {
            int middleIndex = (leftIndex + rightIndex) / 2;
            sort(array, leftIndex, middleIndex);
            sort(array, middleIndex + 1, rightIndex);
            merge(array, leftIndex, middleIndex, rightIndex);
        }
    }

    private void merge(Object[] array, int leftIndex, int middleIndex, int rightIndex) {
        Object[] leftTmpArray = Arrays.copyOfRange(array, leftIndex, middleIndex + 1);
        Object[] rightTmpArray = Arrays.copyOfRange(array, middleIndex + 1, rightIndex + 1);
        merge(array, leftIndex, leftTmpArray, rightTmpArray);
    }

    private void merge(Object[] resultArray, int leftIndex, Object[] array1, Object[] array2) {
        int leftPartIter = 0;
        int rightPartIter = 0;
        int arrayIter = leftIndex;

        while (leftPartIter < array1.length && rightPartIter < array2.length) {
            if (biPredicate.test(array1[leftPartIter], array2[rightPartIter])) {
                resultArray[arrayIter] = array1[leftPartIter];
                leftPartIter++;
            } else {
                resultArray[arrayIter] = array2[rightPartIter];
                rightPartIter++;
            }
            arrayIter++;
        }
        while (leftPartIter < array1.length) {
            resultArray[arrayIter] = array1[leftPartIter];
            leftPartIter++;
            arrayIter++;
        }
        while (rightPartIter < array2.length) {
            resultArray[arrayIter] = array2[rightPartIter];
            rightPartIter++;
            arrayIter++;
        }
    }
}

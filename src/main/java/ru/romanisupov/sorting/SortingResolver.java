package ru.romanisupov.sorting;

import ru.romanisupov.configuration.DataType;
import ru.romanisupov.configuration.SortingMode;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BiPredicate;

public class SortingResolver {

    private final Map<DataType, Map<SortingMode, Sorter>> sorters;

    public SortingResolver() {
        sorters = new EnumMap<>(DataType.class);
        sorters.put(DataType.INTEGER, integerSorters());
        sorters.put(DataType.STRING, stringSorters());
    }

    public List<Object> resolve(List<Object[]> values, DataType dataType, SortingMode sortingMode) {
        var sorter = sorters.get(dataType).get(sortingMode);
        LinkedList<Object[]> valuesCopy = new LinkedList<>(values);
        return sort(valuesCopy, sorter);
    }

    private List<Object> sort(LinkedList<Object[]> values, Sorter sorter) {
        Object[] resultArray = values.pop();
        sorter.sort(resultArray);
        while (!values.isEmpty()) {
            Object[] array = values.pop();
            sorter.sort(array);
            resultArray = sorter.merge(resultArray, array);
        }
        return Arrays.asList(resultArray);
    }

    private Map<SortingMode, Sorter> integerSorters() {
        Map<SortingMode, Sorter> integerSorters = new EnumMap<>(SortingMode.class);
        integerSorters.put(SortingMode.ASC, ascendingIntegerSorter());
        integerSorters.put(SortingMode.DESC, descendingIntegerSorter());
        return integerSorters;
    }

    private Sorter ascendingIntegerSorter() {
        BiPredicate<Object, Object> biPredicate = (obj1, obj2) -> {
            Integer int1 = (Integer) obj1;
            Integer int2 = (Integer) obj2;
            return int1 < int2;
        };
        return new Sorter(biPredicate);
    }

    private Sorter descendingIntegerSorter() {
        BiPredicate<Object, Object> biPredicate = (obj1, obj2) -> {
            Integer int1 = (Integer) obj1;
            Integer int2 = (Integer) obj2;
            return int1 > int2;
        };
        return new Sorter(biPredicate);
    }

    private Map<SortingMode, Sorter> stringSorters() {
        Map<SortingMode, Sorter> stringSorters = new EnumMap<>(SortingMode.class);
        stringSorters.put(SortingMode.ASC, ascendingStringSorter());
        stringSorters.put(SortingMode.DESC, descendingStringSorter());
        return stringSorters;
    }

    private Sorter ascendingStringSorter() {
        BiPredicate<Object, Object> biPredicate = (obj1, obj2) -> {
            String str1 = (String) obj1;
            String str2 = (String) obj2;
            return str1.compareTo(str2) <= 0;
        };
        return new Sorter(biPredicate);
    }

    private Sorter descendingStringSorter() {
        BiPredicate<Object, Object> biPredicate = (obj1, obj2) -> {
            String str1 = (String) obj1;
            String str2 = (String) obj2;
            return str1.compareTo(str2) > 0;
        };
        return new Sorter(biPredicate);
    }
}

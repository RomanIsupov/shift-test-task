package ru.romanisupov.configuration;

public enum DataType {

    INTEGER,

    STRING
}

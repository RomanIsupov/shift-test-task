package ru.romanisupov.configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.nio.file.Path;
import java.util.List;

@Getter
@AllArgsConstructor
public class Configuration {

    private final SortingMode sortingMode;

    private final DataType dataType;

    private final Path outputFilePath;

    private final List<Path> inputFilesPaths;
}

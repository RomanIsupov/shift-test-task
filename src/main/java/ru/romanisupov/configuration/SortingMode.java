package ru.romanisupov.configuration;

public enum SortingMode {

    ASC,

    DESC
}

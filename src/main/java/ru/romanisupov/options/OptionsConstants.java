package ru.romanisupov.options;

import lombok.experimental.UtilityClass;

@UtilityClass
class OptionsConstants {

    static final int SORTING_MODE_INDEX = 0;

    static final int DATA_TYPE_INDEX_WITHOUT_SORTING_MODE = 0;

    static final int DATA_TYPE_INDEX_WITH_SORTING_MODE = 1;

    static final int OUTPUT_FILE_INDEX_WITHOUT_SORTING_MODE = 1;

    static final int OUTPUT_FILE_INDEX_WITH_SORTING_MODE = 2;

    static final int INPUT_FILES_START_INDEX_WITHOUT_SORTING_MODE = 2;

    static final int INPUT_FILES_START_INDEX_WITH_SORTING_MODE = 3;
}

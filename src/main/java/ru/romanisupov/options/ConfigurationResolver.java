package ru.romanisupov.options;

import ru.romanisupov.configuration.Configuration;
import ru.romanisupov.configuration.DataType;
import ru.romanisupov.configuration.SortingMode;
import ru.romanisupov.exception.IncorrectOptionException;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

final class ConfigurationResolver {

    Configuration resolve(
            String sortingModeOption,
            String dataTypeOption,
            String outputFileOption,
            List<String> inputFilesOptions
    ) {
        var sortingMode = sortingMode(sortingModeOption);
        var dataType = dataType(dataTypeOption);
        var outputFilePath = outputFilePath(outputFileOption);
        var inputFilesPaths = inputFilesPaths(inputFilesOptions);
        return new Configuration(sortingMode, dataType, outputFilePath, inputFilesPaths);
    }

    private SortingMode sortingMode(String option) {
        if (Objects.equals(option, "") || Objects.equals(option, "-a")) {
            return SortingMode.ASC;
        } else if (Objects.equals(option, "-d")) {
            return SortingMode.DESC;
        } else {
            throw new IncorrectOptionException(option);
        }
    }

    private DataType dataType(String option) {
        if (Objects.equals(option, "-i")) {
            return DataType.INTEGER;
        } else if (Objects.equals(option, "-s")) {
            return DataType.STRING;
        } else {
            throw new IncorrectOptionException(option);
        }
    }

    private List<Path> inputFilesPaths(List<String> options) {
        return options.stream()
                .map(this::getPath)
                .collect(Collectors.toList());
    }

    private Path outputFilePath(String option) {
        return getPath(option);
    }

    private Path getPath(String pathOption) {
        try {
            return Paths.get(pathOption);
        } catch (InvalidPathException e) {
            throw new ru.romanisupov.exception.InvalidPathException(pathOption);
        }
    }
}

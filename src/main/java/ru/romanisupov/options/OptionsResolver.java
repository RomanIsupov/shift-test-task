package ru.romanisupov.options;

import ru.romanisupov.configuration.Configuration;
import ru.romanisupov.exception.EmptyOptionsException;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static ru.romanisupov.options.OptionsConstants.DATA_TYPE_INDEX_WITHOUT_SORTING_MODE;
import static ru.romanisupov.options.OptionsConstants.DATA_TYPE_INDEX_WITH_SORTING_MODE;
import static ru.romanisupov.options.OptionsConstants.INPUT_FILES_START_INDEX_WITHOUT_SORTING_MODE;
import static ru.romanisupov.options.OptionsConstants.INPUT_FILES_START_INDEX_WITH_SORTING_MODE;
import static ru.romanisupov.options.OptionsConstants.OUTPUT_FILE_INDEX_WITHOUT_SORTING_MODE;
import static ru.romanisupov.options.OptionsConstants.OUTPUT_FILE_INDEX_WITH_SORTING_MODE;
import static ru.romanisupov.options.OptionsConstants.SORTING_MODE_INDEX;

public class OptionsResolver {

    private final ConfigurationResolver configurationResolver;

    public OptionsResolver() {
        this.configurationResolver = new ConfigurationResolver();
    }

    public Configuration resolve(String[] options) {
        validateOptions(options);
        String sortingModeOption = getSortingModeOption(options);
        String dataTypeOption = getDataTypeOption(options);
        String outputFileOption = getOutputFileOption(options);
        List<String> inputFilesOptions = getInputFilesOptions(options);
        return configurationResolver.resolve(sortingModeOption, dataTypeOption, outputFileOption, inputFilesOptions);
    }

    private void validateOptions(String[] options) {
        if (Objects.isNull(options) || options.length == 0) {
            throw new EmptyOptionsException();
        }
    }

    private boolean containsSortingMode(String[] options) {
        String option = options[SORTING_MODE_INDEX];
        return Objects.equals(option, "-a") || Objects.equals(option, "-d");
    }

    private String getSortingModeOption(String[] options) {
        if (containsSortingMode(options)) {
            return options[SORTING_MODE_INDEX];
        }
        return "";
    }

    private String getDataTypeOption(String[] options) {
        if (containsSortingMode(options)) {
            return options[DATA_TYPE_INDEX_WITH_SORTING_MODE];
        }
        return options[DATA_TYPE_INDEX_WITHOUT_SORTING_MODE];
    }

    private String getOutputFileOption(String[] options) {
        if (containsSortingMode(options)) {
            return options[OUTPUT_FILE_INDEX_WITH_SORTING_MODE];
        }
        return options[OUTPUT_FILE_INDEX_WITHOUT_SORTING_MODE];
    }

    private List<String> selectInputFilesOptionsAsList(String[] options, int startIndex) {
        String[] inputFilesOptions = Arrays.copyOfRange(options, startIndex, options.length);
        return Arrays.asList(inputFilesOptions);
    }

    private List<String> getInputFilesOptions(String[] options) {
        if (containsSortingMode(options)) {
            return selectInputFilesOptionsAsList(options, INPUT_FILES_START_INDEX_WITH_SORTING_MODE);
        }
        return selectInputFilesOptionsAsList(options, INPUT_FILES_START_INDEX_WITHOUT_SORTING_MODE);
    }
}

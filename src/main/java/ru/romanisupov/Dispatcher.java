package ru.romanisupov;

import ru.romanisupov.exception.ApplicationException;
import ru.romanisupov.files.FilesResolver;
import ru.romanisupov.options.OptionsResolver;
import ru.romanisupov.sorting.SortingResolver;

import java.util.Objects;
import java.util.stream.Collectors;

public class Dispatcher {

    private final OptionsResolver optionsResolver;

    private final FilesResolver filesResolver;

    private final SortingResolver sortingResolver;

    Dispatcher() {
        optionsResolver = new OptionsResolver();
        filesResolver = new FilesResolver();
        sortingResolver = new SortingResolver();
    }

    public void dispatch(String[] args) {
        try {
            var configuration = optionsResolver.resolve(args);
            var values = filesResolver.resolveReading(
                    configuration.getInputFilesPaths(),
                    configuration.getDataType());
            var sortedValues = sortingResolver.resolve(
                    values,
                    configuration.getDataType(),
                    configuration.getSortingMode());
            var sortedValuesAsStrings = sortedValues.stream()
                    .map(Objects::toString)
                    .collect(Collectors.toList());
            filesResolver.resolveWriting(configuration.getOutputFilePath(), sortedValuesAsStrings);
        } catch (ApplicationException e) {
            handleException(e);
        }
    }

    private void handleException(ApplicationException e) {
        System.out.println(e.getMessage());
    }
}

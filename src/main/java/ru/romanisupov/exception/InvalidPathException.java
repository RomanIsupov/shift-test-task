package ru.romanisupov.exception;

import static ru.romanisupov.exception.ExceptionMessages.INVALID_PATH;

public class InvalidPathException extends ApplicationException {

    public InvalidPathException(String path) {
        super(String.format(INVALID_PATH, path));
    }
}

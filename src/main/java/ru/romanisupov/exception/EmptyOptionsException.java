package ru.romanisupov.exception;

import static ru.romanisupov.exception.ExceptionMessages.EMPTY_OPTIONS;

public class EmptyOptionsException extends ApplicationException {

    public EmptyOptionsException() {
        super(EMPTY_OPTIONS);
    }
}

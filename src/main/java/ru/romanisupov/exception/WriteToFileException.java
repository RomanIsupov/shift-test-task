package ru.romanisupov.exception;

import java.nio.file.Path;

import static ru.romanisupov.exception.ExceptionMessages.WRITE_TO_FILE;

public class WriteToFileException extends InternalException {

    public WriteToFileException(Path path) {
        super(String.format(WRITE_TO_FILE, path));
    }
}

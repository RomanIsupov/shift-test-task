package ru.romanisupov.exception;

import lombok.experimental.UtilityClass;

@UtilityClass
class ExceptionMessages {

    static final String CREATE_FILE = "error while creating file `%s`";

    static final String EMPTY_OPTIONS = "Options cannot be empty";

    static final String INCORRECT_OPTION = "Incorrect option `%s`";

    static final String INCORRECT_VALUE = "Incorrect value `%s` will be skipped";

    static final String INTERNAL = "Something went wrong: %s";

    static final String INVALID_PATH = "path `%s` is invalid";

    static final String READ_FILE = "error while reading file `%s`";

    static final String TYPE_NOT_SPECIFIED = "Type not specified";

    static final String UNSUPPORTED_TYPE = "Type is not supported";

    static final String WRITE_TO_FILE = "error while writing to file `%s`";
}

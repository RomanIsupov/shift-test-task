package ru.romanisupov.exception;

import static ru.romanisupov.exception.ExceptionMessages.TYPE_NOT_SPECIFIED;

public class TypeNotSpecifiedException extends ApplicationException {

    public TypeNotSpecifiedException() {
        super(TYPE_NOT_SPECIFIED);
    }
}

package ru.romanisupov.exception;

import java.nio.file.Path;

import static ru.romanisupov.exception.ExceptionMessages.CREATE_FILE;

public class CreateFileException extends InternalException {

    public CreateFileException(Path path) {
        super(String.format(CREATE_FILE, path));
    }
}

package ru.romanisupov.exception;

import static ru.romanisupov.exception.ExceptionMessages.INCORRECT_OPTION;

public class IncorrectOptionException extends ApplicationException {

    public IncorrectOptionException(String option) {
        super(String.format(INCORRECT_OPTION, option));
    }
}

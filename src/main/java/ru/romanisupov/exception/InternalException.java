package ru.romanisupov.exception;

import static ru.romanisupov.exception.ExceptionMessages.INTERNAL;

public class InternalException extends ApplicationException {

    public InternalException(String message) {
        super(String.format(INTERNAL, message));
    }
}

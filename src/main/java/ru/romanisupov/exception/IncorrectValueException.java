package ru.romanisupov.exception;

import static ru.romanisupov.exception.ExceptionMessages.INCORRECT_VALUE;

public class IncorrectValueException extends ApplicationException {

    public IncorrectValueException(String value) {
        super(String.format(INCORRECT_VALUE, value));
    }
}

package ru.romanisupov.exception;

import java.nio.file.Path;

import static ru.romanisupov.exception.ExceptionMessages.READ_FILE;

public class ReadFileException extends InternalException {

    public ReadFileException(Path path) {
        super(String.format(READ_FILE, path));
    }
}
